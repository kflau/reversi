package com.aei.reversi.domain;

public enum Disc {

    NO_DISC("-"),
    LIGHT("O"),
    DARK("X");

    private String symbol;

    Disc(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
