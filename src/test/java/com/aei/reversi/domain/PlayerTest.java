package com.aei.reversi.domain;

import com.aei.reversi.service.UserInputs;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PlayerTest {

    private Player subject;
    private Player opponent;

    @Mock
    private Board board;
    @Mock
    private Board anotherBoard;
    @Mock
    private UserInputs userInputs;

    private List<Coordinate> matrix;
    private List<Coordinate> matrixWithEdgeLeft;
    private List<Character> alphabets;
    private Integer width = 8;
    private Integer height = 8;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        subject = new Player(Disc.DARK, board, userInputs);
        opponent = new Player(Disc.LIGHT, board, userInputs);

        Coordinate d4 = new Coordinate('d', 4);
        Coordinate e5 = new Coordinate('e', 5);
        d4.setDisc(Disc.LIGHT);
        e5.setDisc(Disc.LIGHT);
        Coordinate e4 = new Coordinate('e', 4);
        Coordinate d5 = new Coordinate('d', 5);
        e4.setDisc(Disc.DARK);
        d5.setDisc(Disc.DARK);

        alphabets = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');

        List<Coordinate> darkPool = new ArrayList<>();
        for (int i = 2; i <= 7; i++) {
            for (int j = 1; j < alphabets.size() - 1; j++) {
                Coordinate coordinate = new Coordinate(alphabets.get(j), i);
                coordinate.setDisc(Disc.DARK);
                darkPool.add(coordinate);
            }
        }

        matrix = Arrays.asList(
                new Coordinate('a', 1), new Coordinate('b', 1), new Coordinate('c', 1), new Coordinate('d', 1), new Coordinate('e', 1), new Coordinate('f', 1), new Coordinate('g', 1), new Coordinate('h', 1),
                new Coordinate('a', 2), new Coordinate('b', 2), new Coordinate('c', 2), new Coordinate('d', 2), new Coordinate('e', 2), new Coordinate('f', 2), new Coordinate('g', 2), new Coordinate('h', 2),
                new Coordinate('a', 3), new Coordinate('b', 3), new Coordinate('c', 3), new Coordinate('d', 3), new Coordinate('e', 3), new Coordinate('f', 3), new Coordinate('g', 3), new Coordinate('h', 3),
                new Coordinate('a', 4), new Coordinate('b', 4), new Coordinate('c', 4), d4, e4, new Coordinate('f', 4), new Coordinate('g', 4), new Coordinate('h', 4),
                new Coordinate('a', 5), new Coordinate('b', 5), new Coordinate('c', 5), d5, e5, new Coordinate('f', 5), new Coordinate('g', 5), new Coordinate('h', 5),
                new Coordinate('a', 6), new Coordinate('b', 6), new Coordinate('c', 6), new Coordinate('d', 6), new Coordinate('e', 6), new Coordinate('f', 6), new Coordinate('g', 6), new Coordinate('h', 6),
                new Coordinate('a', 7), new Coordinate('b', 7), new Coordinate('c', 7), new Coordinate('d', 7), new Coordinate('e', 7), new Coordinate('f', 7), new Coordinate('g', 7), new Coordinate('h', 7),
                new Coordinate('a', 8), new Coordinate('b', 8), new Coordinate('c', 8), new Coordinate('d', 8), new Coordinate('e', 8), new Coordinate('f', 8), new Coordinate('g', 8), new Coordinate('h', 8));
        darkPool.addAll(Arrays.asList(
                new Coordinate('a', 1), new Coordinate('b', 1), new Coordinate('c', 1), new Coordinate('d', 1), new Coordinate('e', 1), new Coordinate('f', 1), new Coordinate('g', 1), new Coordinate('h', 1),
                new Coordinate('a', 2), new Coordinate('h', 2),
                new Coordinate('a', 3), new Coordinate('h', 3),
                new Coordinate('a', 4), new Coordinate('h', 4),
                new Coordinate('a', 5), new Coordinate('h', 5),
                new Coordinate('a', 6), new Coordinate('h', 6),
                new Coordinate('a', 7), new Coordinate('h', 7),
                new Coordinate('a', 8), new Coordinate('b', 8), new Coordinate('c', 8), new Coordinate('d', 8), new Coordinate('e', 8), new Coordinate('f', 8), new Coordinate('g', 8), new Coordinate('h', 8)));
        Collections.sort(darkPool);
        matrixWithEdgeLeft = darkPool;
    }

    @Test
    public void move_emptyCoordinate() {
        when(board.remainingCoordinates()).thenReturn(0L);
        subject.move("");
        verify(board, times(1)).remainingCoordinates();
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateOutsideBoundaryA99() {
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getHeight()).thenReturn(height);
        subject.move("a99");
        verify(board, times(1)).getAlphabets();
        verify(board, times(1)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateOutsideBoundaryZ1() {
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        subject.move("z1");
        verify(board, times(1)).getAlphabets();
        verify(board, times(1)).remainingCoordinates();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateOutsideBoundary1Z() {
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        subject.move("1z");
        verify(board, times(1)).getAlphabets();
        verify(board, times(1)).remainingCoordinates();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateInsideBoundaryA1() {
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getHeight()).thenReturn(height);
        when(board.getWidth()).thenReturn(width);
        subject.move("a1");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(1)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateInsideBoundary1A() {
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getHeight()).thenReturn(height);
        when(board.getWidth()).thenReturn(width);
        subject.move("1a");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(1)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateAdjacentDiscTopLeftCorner() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("1a");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(1)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateAdjacentDiscTopRightCorner() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("h1");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(1)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateAdjacentDiscBottomLeftCorner() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("a8");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(1)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateAdjacentDiscBottomRightCorner() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(0L);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("h8");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(1)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateD3Available() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("d3");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, times(1)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinate4DUnavailable() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("4d");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(2)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateOccupied() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("d4");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(1)).remainingCoordinates();
        verify(board, times(2)).getMatrix();
        verify(board, times(1)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinate4DInvalidInput() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.remainingCoordinates()).thenReturn(1L).thenReturn(0L);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("4d");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(2)).remainingCoordinates();
        verify(board, times(3)).getMatrix();
        verify(board, times(2)).getWidth();
        verify(userInputs, times(1)).retrievePlayerInput();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingUp() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("e6");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, times(1)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingUpLeft() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("e6");
        opponent.move("f6");
        verify(board, times(2)).getAlphabets();
        verify(board, times(4)).getHeight();
        verify(board, times(24)).getMatrix();
        verify(board, times(22)).getWidth();
        verify(board, times(2)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingUpRight() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        opponent.move("d6");
        subject.move("c6");
        verify(board, times(2)).getAlphabets();
        verify(board, times(4)).getHeight();
        verify(board, times(24)).getMatrix();
        verify(board, times(22)).getWidth();
        verify(board, times(2)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateAtRightEdgeGoingUpRight() {
        when(board.getMatrix()).thenReturn(matrixWithEdgeLeft);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("h6");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(11)).getMatrix();
        verify(board, times(10)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingDown() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("3d");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, times(1)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateAtLeftEdgeGoingDownRight() {
        when(board.getMatrix()).thenReturn(matrixWithEdgeLeft);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("6a");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(11)).getMatrix();
        verify(board, times(10)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingDownLeft() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        opponent.move("3e");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, times(1)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingDownRight() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("d3");
        opponent.move("c3");
        verify(board, times(2)).getAlphabets();
        verify(board, times(4)).getHeight();
        verify(board, times(24)).getMatrix();
        verify(board, times(22)).getWidth();
        verify(board, times(2)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingLeft() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("f5");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, times(1)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingRight() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("c4");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, times(1)).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void move_coordinateGoingRightWithoutCapturing() {
        when(board.getMatrix()).thenReturn(matrix);
        when(board.getAlphabets()).thenReturn(alphabets);
        when(board.getWidth()).thenReturn(width);
        when(board.getHeight()).thenReturn(height);
        subject.move("f4");
        verify(board, times(1)).getAlphabets();
        verify(board, times(2)).getHeight();
        verify(board, times(12)).getMatrix();
        verify(board, times(11)).getWidth();
        verify(board, never()).place(any(Coordinate.class));
        verifyNoMoreInteractions(userInputs, board);
    }

    @Test
    public void getDisc() {
        Assert.assertEquals("Player Disc defaults to Dark", subject.getDisc(), Disc.DARK);
    }

    @Test
    public void equals_sameMemoryAddress() {
        Assert.assertSame("Player with the same memory address must be equal", subject, subject);
    }

    @Test
    public void equals_differentMemoryAddressNotEqual() {
        Assert.assertNotSame("Player with the different memory address cannot be equal", subject, new Player(Disc.DARK, board, userInputs));
    }

    @Test
    public void equals_nullCoordinateNotEqual() {
        Assert.assertNotEquals("Null coordinate cannot be equal", subject, null);
    }

    @Test
    public void equals_differentClassNotEqual() {
        Assert.assertNotEquals("Different classes cannot be equal", subject, new Object());
    }

    @Test
    public void equals_differentDiscAndSameBoardPlayerNotEqual() {
        Assert.assertNotEquals("Players with the different disc and same board cannot be equal", subject, new Player(Disc.LIGHT, board, userInputs));
    }

    @Test
    public void equals_sameDiscAndDifferentBoardPlayerNotEqual() {
        Assert.assertNotEquals("Players with the same disc and different board cannot be equal", subject, new Player(Disc.DARK, anotherBoard, userInputs));
    }

    @Test
    public void equals_sameDiscAndBoardPlayerEqual() {
        Assert.assertEquals("Players with the same disc and board must be equal", subject, new Player(Disc.DARK, board, userInputs));
    }

    @Test
    public void hashCode_ofDarkPlayer() {
        Assert.assertEquals("Dark player with the same board always the same", subject.hashCode(), new Player(Disc.DARK, board, userInputs).hashCode());
    }

    @Test
    public void toString_ofDarkPlayer() {
        Assert.assertEquals("Dark player must have the same symbol", subject.toString(), new Player(Disc.DARK, board, userInputs).toString());
    }
}