package com.aei.reversi.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CoordinateTest {

    private Coordinate subject;

    @Before
    public void setUp() {
        subject = new Coordinate('d', 4);
        subject.setDisc(Disc.DARK);
    }

    @Test
    public void equals_sameMemoryAddressMustEqual() {
        Assert.assertSame("Coordinates with the same memory address must be equal", subject, subject);
    }

    @Test
    public void equals_differentMemoryAddressNotEqual() {
        Assert.assertNotSame("Coordinates with the different memory address cannot be equal", subject, new Coordinate('a', 1));
    }

    @Test
    public void equals_nullCoordinateNotEqual() {
        Assert.assertNotEquals("Null coordinate cannot be equal", subject, null);
    }

    @Test
    public void equals_differentClassNotEqual() {
        Assert.assertNotEquals("Different classes cannot be equal", subject, new Object());
    }

    @Test
    public void equals_sameXYCoordinateEqual() {
        Assert.assertEquals("Coordinates with the same XY must be equal", subject, new Coordinate('d', 4));
    }

    @Test
    public void equals_differentXCoordinateNotEqual() {
        Assert.assertNotEquals("Coordinates with the different X must not be equal", subject, new Coordinate('c', 1));
    }

    @Test
    public void equals_differentYCoordinateNotEqual() {
        Assert.assertNotEquals("Coordinates with the different Y must not be equal", subject, new Coordinate('a', 8));
    }

    @Test
    public void hashCode_ofD4() {
        Assert.assertEquals("Coordinate A1 always the same", subject.hashCode(), new Coordinate('d', 4).hashCode());
    }

    @Test
    public void toString_ofDark() {
        Assert.assertEquals("Stringify coordinate with dark disc", subject.toString(), Disc.DARK.toString());
    }

    @Test
    public void compareTo_lowerLine() {
        Assert.assertEquals("Compare coordinate underneath", subject.compareTo(new Coordinate('h', 5)), -1);
    }

    @Test
    public void compareTo_upperLine() {
        Assert.assertEquals("Compare coordinate above", subject.compareTo(new Coordinate('a', 3)), 1);
    }

    @Test
    public void compareTo_sameLineButLargerAsciiLetter() {
        Assert.assertEquals("Compare coordinate to the same line but larger ASCII letter", subject.compareTo(new Coordinate('g', 4)), -1);
    }

    @Test
    public void compareTo_sameLineButLowerAsciiLetter() {
        Assert.assertEquals("Compare coordinate to the same line but lower ASCII letter", subject.compareTo(new Coordinate('b', 4)), 1);
    }

    @Test
    public void compareTo_sameCoordinate() {
        Assert.assertEquals("Compare coordinate to the same coordinate", subject.compareTo(new Coordinate('d', 4)), 0);
    }
}