package com.aei.reversi.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BoardTest {

    private Board subject;

    @Before
    public void setUp() {
        subject = new Board(8, 8, Arrays.asList(28, 35), Arrays.asList(27, 36));
    }

    @Test()
    public void Board_widthOf8() {
        subject = new Board(8, 8, Arrays.asList(28, 35), Arrays.asList(27, 36));
        Assert.assertEquals("Width is of size 8", subject.getWidth().intValue(), 8);
    }

    @Test(expected = RuntimeException.class)
    public void Board_widthLargerThan26() {
        subject = new Board(30, 8, Arrays.asList(28, 35), Arrays.asList(27, 36));
    }

    @Test
    public void initialize_totalOf64() {
        subject.initialize();
        Assert.assertEquals("Total Discs is 64", 64, subject.getMatrix().size());
    }

    @Test
    public void initialize_alphabetsOf8() {
        subject.initialize();
        Assert.assertEquals("Total alphabets is 8", 8, subject.getAlphabets().size());
    }

    @Test
    public void initialize_matrixContainsEqualLightAndDarkDiscs() {
        subject.initialize();
        Assert.assertEquals("Matrix contains equal amount of light and dark discs",
                subject.getMatrix().stream().filter(coordinate -> Disc.LIGHT.equals(coordinate.getDisc())).count(),
                subject.getMatrix().stream().filter(coordinate -> Disc.DARK.equals(coordinate.getDisc())).count());
    }

    @Test
    public void getWidth() {
        Assert.assertEquals("Width is of size 8", subject.getWidth().intValue(), 8);
    }

    @Test
    public void getHeight() {
        Assert.assertEquals("Height is of size 8", subject.getHeight().intValue(), 8);
    }

    @Test
    public void place() {
        subject.initialize();
        Coordinate coordinate = subject.getMatrix().get(0);
        coordinate.setDisc(Disc.DARK);
        List<Coordinate> straightLines = new ArrayList<>();
        straightLines.add(coordinate);
        subject.place(straightLines.toArray(new Coordinate[0]));
        Assert.assertEquals("First coordinate is set to Dark", subject.getMatrix().get(0).getDisc(), Disc.DARK);
    }

    @Test
    public void remainingCoordinates() {
        subject.initialize();
        Assert.assertEquals("Remaining coordinates must be enough to process", subject.remainingCoordinates().intValue(), 60);
    }
}