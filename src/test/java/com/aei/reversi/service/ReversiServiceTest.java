package com.aei.reversi.service;

import com.aei.reversi.domain.Disc;
import com.aei.reversi.domain.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReversiServiceTest {

    private ReversiService subject;

    @Mock
    private UserInputs userInputs;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        subject = ReversiService.getInstance();
    }

    @Test
    public void getInstance_sameInstanceReturned() {
        ReversiService instance1 = ReversiService.getInstance();
        ReversiService instance2 = ReversiService.getInstance();
        Assert.assertEquals("Multiple getInstance always return the same ReversiService", instance1, instance2);
    }

    @Test
    public void start_lightPlayerWins() {
        when(userInputs.retrievePlayerInput())
                .thenReturn("3d")
                .thenReturn("4d")
                .thenReturn("b1")
                .thenReturn("c1")
                .thenReturn("d1")
                .thenReturn("a1")
                .thenReturn("2a")
                .thenReturn("2d")
                .thenReturn("4c")
                .thenReturn("3a")
                .thenReturn("4a")
                .thenReturn("4b");
        Player winner = subject.start(4, 4, Arrays.asList(6, 9), Arrays.asList(5, 10), userInputs);
        Assert.assertEquals("Winner is light player", winner.getDisc(), Disc.LIGHT);
        verify(userInputs, times(12)).retrievePlayerInput();
        verifyNoMoreInteractions(userInputs);
    }

    @Test
    public void start_lightPlayerWinsWithOneSideDisc() {
        when(userInputs.retrievePlayerInput())
                .thenReturn("4b")
                .thenReturn("3a")
                .thenReturn("4b")
                .thenReturn("d2");
        Player winner = subject.start(4, 4, Arrays.asList(6, 9), Arrays.asList(5, 10), userInputs);
        Assert.assertEquals("Winner is light player with one side disc", winner.getDisc(), Disc.LIGHT);
        verify(userInputs, times(4)).retrievePlayerInput();
        verifyNoMoreInteractions(userInputs);
    }

    @Test
    public void start_darkPlayerWinsWithOneSideDisc() {
        when(userInputs.retrievePlayerInput())
                .thenReturn("3d")
                .thenReturn("2a")
                .thenReturn("2a");
        Player winner = subject.start(4, 4, Arrays.asList(6, 9), Arrays.asList(5, 10), userInputs);
        Assert.assertEquals("Winner is dark player with one side disc", winner.getDisc(), Disc.DARK);
        verify(userInputs, times(3)).retrievePlayerInput();
        verifyNoMoreInteractions(userInputs);
  }
}